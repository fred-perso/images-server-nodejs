var restify = require('restify');
var errors = require('restify-errors');
var bunyan = require('bunyan');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

const server = restify.createServer({
    name: 'images',
    version: '1.0.0'
});

var trials = {};

server.pre(function (req, res, next) {
    req.headers.accept = 'application/json';
    return next();
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser({uploadDir: './public/images'}));


var log = bunyan.createLogger({
    name: 'images',
    streams: [
        {
            level: 'debug',
            stream: process.stdout            // log INFO and above to stdout
        },
    ]
});


server.get('/images/labels-histogram', function (req, res, next) {

    labelsMap = new Map();
    labels = new Array();

    if (req.query.labels) {
        console.log(req.query.labels);
        labels = req.query.labels.split(',');

    } else {
        const files = fs.readdirSync('./public/images/');
        files.forEach((file) => {
            isDirectory = fs.statSync('./public/images/' + file).isDirectory()
            if (isDirectory) {
                labels.push(file)
            }
        })
    }

    labels.forEach((label) => {
        dirFiles = fs.readdirSync('./public/images/' + label);
        console.log(dirFiles.length);
        labelsMap.set(label, dirFiles.length);
    })


    let obj = Array.from(labelsMap).reduce((obj, [key, value]) => (
        Object.assign(obj, {[key]: value}) // Be careful! Maps can have non-String keys; object literals can't.
    ), {});

    res.send(obj);

});


server.post('/images', function (req, res, next) {
    const imagePath = req.files.picture.path;
    var label = "";
    if (req.body.label) {
        label = req.body.label + '/';
    }

    const newPath = "./public/images/" + label;
    if (!fs.existsSync(newPath)) {
        console.log("Creating new directory: " + newPath);
        fs.mkdirSync(newPath);
    }

    const imageUUID = uuidv1()
    const imageName = imageUUID + '.jpg'

    fs.rename(imagePath, newPath + imageName, function (err) {
        if (err) console.log(err);
    })

    console.log("Label: " + label);
    console.log(imagePath);
    res.send(201);
    return next();

});

function uploadImage(imagePath, imageName, bucketPath, metadata) {
    return new Promise((resolve, reject) => {
        if (imageName == undefined) {
            imageName = path.basename(imagePath)
        }

        log.debug('Uploading image: ' + imagePath + " imageName: " + imageName)

        var fileStream = fs.createReadStream(imagePath);
        fileStream.on('error', function (err) {
            log.debug('File Error', err);
        });
    })

}


server.listen(9004, function () {
    log.info('%s listening at %s', server.name, server.url);
});
